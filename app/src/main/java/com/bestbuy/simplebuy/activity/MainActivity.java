package com.bestbuy.simplebuy.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bestbuy.simplebuy.R;
import com.bestbuy.simplebuy.adapter.ProductListAdapter;
import com.bestbuy.simplebuy.model.Product;
import com.bestbuy.simplebuy.viewmodel.PLPViewModel;

import java.util.List;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    private RecyclerView productRecyclerView;
    private Button findProductsButton;
    private TextView cartTotal;

    private PLPViewModel viewModel;
    private ProductListAdapter productListAdapter;

    private ProductListAdapter.OnProductSelectListener productSelectListener = new ProductListAdapter.OnProductSelectListener() {
        @Override
        public void onProductSelected(Product product) {
            viewModel.onProductAddedToCart(product);

        }
    };

    private View.OnClickListener findProductsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            viewModel.fetchProducts();
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewModel = ViewModelProviders.of(this).get(PLPViewModel.class);
        productRecyclerView = findViewById(R.id.recycler_view);
        findProductsButton = findViewById(R.id.fetch_button);
        cartTotal = findViewById(R.id.cart_total);

        final List<Product> products = viewModel.getLiveProducts().getValue();
        final Set<Product> productsInCart = viewModel.getLiveProductsInCart().getValue();
        productListAdapter = new ProductListAdapter(products, productsInCart, productSelectListener);
        productRecyclerView.setAdapter(productListAdapter);

        observeProducts();
        observeCart();

        findProductsButton.setOnClickListener(findProductsClickListener);

    }

    private void observeProducts() {
        viewModel.getLiveProducts().observe(this, new Observer<List<Product>>() {
            @Override
            public void onChanged(@Nullable List<Product> products) {
                productListAdapter.setProducts(products);
            }
        });
    }

    private void observeCart() {
        viewModel.getLiveProductsInCart().observe(this, new Observer<Set<Product>>() {
            @Override
            public void onChanged(@Nullable Set<Product> productsInCart) {
                productListAdapter.setProductsInCart(productsInCart);
                cartTotal.setText("Total: " + viewModel.getCartTotal());
            }
        });
    }
}
