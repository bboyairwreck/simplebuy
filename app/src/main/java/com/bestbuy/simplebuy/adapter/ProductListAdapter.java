package com.bestbuy.simplebuy.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bestbuy.simplebuy.R;
import com.bestbuy.simplebuy.model.Product;

import java.util.List;
import java.util.Set;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ProductViewHolder> {

    private List<Product> products;
    private Set<Product> productsInCart;
    private OnProductSelectListener productSelectListener;

    public ProductListAdapter(List<Product> products, Set<Product> productsInCart, OnProductSelectListener productSelectListener) {
        this.products = products;
        this.productsInCart = productsInCart;
        this.productSelectListener = productSelectListener;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_item, viewGroup, false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProductViewHolder productViewHolder, int position) {
        final Product product = products.get(position);
        productViewHolder.titleView.setText(product.getTitle());

        productViewHolder.imageView.setImageResource(product.getImageResID());
        productViewHolder.priceView.setText(String.valueOf(product.getPrice()));
        productViewHolder.addToCart.setText(productsInCart.contains(product) ? "Added" : "Add to cart");

        productViewHolder.addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                productViewHolder.addToCart.setText("Added!");
                productSelectListener.onProductSelected(product);
            }
        });
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView titleView;
        TextView priceView;
        Button addToCart;

        ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image);
            titleView = itemView.findViewById(R.id.title);
            priceView = itemView.findViewById(R.id.price);
            addToCart = itemView.findViewById(R.id.add_to_cart);
        }
    }

    public void setProducts(List<Product> products) {
        this.products = products;
        notifyDataSetChanged();
    }

    public void setProductsInCart(Set<Product> productsInCart) {
        this.productsInCart = productsInCart;
        notifyDataSetChanged();
    }

    public interface OnProductSelectListener {
        void onProductSelected(Product product);
    }
}
