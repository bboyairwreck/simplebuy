package com.bestbuy.simplebuy.data;

import android.os.Handler;

import com.bestbuy.simplebuy.R;
import com.bestbuy.simplebuy.model.Product;

import java.util.ArrayList;
import java.util.List;

public class DataProvider {

    private static final int DEFAULT_DELAY = 500;

    public static List<Product> initData() {
        final List<Product> products = new ArrayList<>();

        products.add(new Product("Sony - 70\" Class - LED - X690E Series - 2160p - Smart - 4K UHD TV with HDR", R.drawable.tv1, 999.99, "5900135"));
        products.add(new Product("TCL - 55\" Class - LED - 4 Series - 2160p - Smart - 4K UHD TV with HDR Roku TV", R.drawable.tv2, 397.99, "5878703"));
        products.add(new Product("Samsung - 43\" Class (42.5\" Diag.) - LED - 1080p - Smart - HDTV", R.drawable.tv3, 269.99, "4340402"));
        products.add(new Product("Samsung - 55\" Class - LED - NU7100 Series - 2160p - Smart - 4K UHD TV with HDR", R.drawable.tv4, 549.99, "6200125"));
        products.add(new Product("VIZIO - 32\" Class - LED - D-Series - 1080p - Smart - HDTV", R.drawable.tv5, 179.99, "6202310"));
        products.add(new Product("Insignia - 55\" Class - LED - 1080p - HDTV", R.drawable.tv6, 299.99, "6172325"));
        products.add(new Product("Samsung - 65\" Class - LED - Q6F Series - 2160p - Smart - 4K UHD TV with HDR\n", R.drawable.tv7, 1299.99, "6202109"));
        products.add(new Product("Insignia™ - 39\" Class - LED - 720p - HDTV\n", R.drawable.tv8, 149.99, "6160800"));
        products.add(new Product("Samsung - 55\" Class - LED - NU6900 Series - 2160p - Smart - 4K UHD TV with HDR\n", R.drawable.tv9, 449.99, "6290508"));
        products.add(new Product("Samsung - 50\" Class - LED - NU6900 Series - 2160p - Smart - 4K UHD TV with HDR\n", R.drawable.tv10, 399.99, "6288348"));
        products.add(new Product("Insignia™ - 50\" Class - LED - 1080p - HDTV", R.drawable.tv11, 249.99, "6172324"));

        return products;
    }

    public static void fetchData(final OnResponseListener listener) {
        // Delay calling onResponse to simulate network call
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Notify data received
                listener.onResponse(initData());
            }
        }, DEFAULT_DELAY);
    }

    public interface OnResponseListener {
        void onResponse(List<Product> products);
    }
}
