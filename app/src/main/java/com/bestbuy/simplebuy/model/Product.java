package com.bestbuy.simplebuy.model;

import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;


public class Product {

    @NonNull
    private String title;

    @DrawableRes
    private int imageID;

    private double price;
    private String sku;

    public Product(@NonNull String title, @DrawableRes int imageID, double price, String sku) {
        // null check title

        this.title = title;
        this.imageID = imageID;
        this.price = price;
        this.sku = sku;
    }

    public String getTitle() {
        return title;
    }

    public double getPrice() {
        return price;
    }

    @DrawableRes
    public int getImageResID() {
        return imageID;
    }

    public String getSku() {
        return sku;
    }

    @Override
    public int hashCode(){
        return sku.hashCode(); // for simplicity
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        return obj instanceof  Product && sku.equals(((Product) obj).sku);
    }
}
