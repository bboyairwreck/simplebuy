package com.bestbuy.simplebuy.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.bestbuy.simplebuy.data.DataProvider;
import com.bestbuy.simplebuy.model.Product;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PLPViewModel extends ViewModel {
    private MutableLiveData<List<Product>> liveProducts;
    private MutableLiveData<Set<Product>> liveProductsInCart;

    private double cartTotal = 0.00;

    public void fetchProducts() {
        DataProvider.fetchData(new DataProvider.OnResponseListener() {
            @Override
            public void onResponse(List<Product> products) {
                setProducts(products);
            }
        });
    }

    public MutableLiveData<List<Product>> getLiveProducts() {
        if (liveProducts == null) {
            liveProducts = new MutableLiveData<>();
            liveProducts.setValue(new ArrayList<Product>());
        }
        return liveProducts;
    }

    public void setProducts(List<Product> products) {
        getLiveProducts().setValue(products);
    }

    public void onProductAddedToCart(Product product) {
        cartTotal += product.getPrice();

        Set<Product> productsInCart = getLiveProductsInCart().getValue();
        productsInCart.add(product);
        liveProductsInCart.setValue(productsInCart);

    }

    public MutableLiveData<Set<Product>> getLiveProductsInCart() {
        if (liveProductsInCart == null) {
            liveProductsInCart = new MutableLiveData<>();
            liveProductsInCart.setValue(new HashSet<Product>());
        }

        return liveProductsInCart;
    }

    public double getCartTotal() {
        return cartTotal;
    }
}
