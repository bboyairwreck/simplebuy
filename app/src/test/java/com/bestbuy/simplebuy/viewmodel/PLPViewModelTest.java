package com.bestbuy.simplebuy.viewmodel;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;

import com.bestbuy.simplebuy.model.Product;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Matchers;
import org.mockito.Mock;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.atLeastOnce;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class PLPViewModelTest {

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    PLPViewModel viewModel;

    @Mock
    private Observer<Set<Product>> productsInCartObserver = mock(Observer.class);

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Before
    public void setUp(){
        viewModel = new PLPViewModel();
    }

    @Test
    public void testProductAddedToCart() {
        final List<Product> products = new ArrayList<>();
        final Product product = createDummyProduct();
        products.add(product);

        viewModel.setProducts(products);

        viewModel.onProductAddedToCart(product);
        assertTrue(viewModel.getLiveProductsInCart().getValue().contains(product));
    }

    @Test
    public void testProductCartTotal() {
        final double totalPrice = 3.50;

        final List<Product> products = new ArrayList<>();
        Product product1 = createDummyProduct(1.50);
        Product product2 = createDummyProduct(2.00);
        products.add(product1);
        products.add(product2);

        viewModel.getLiveProductsInCart().observeForever(productsInCartObserver);
        verify(productsInCartObserver, atLeastOnce()).onChanged(Matchers.any(Set.class));

        viewModel.onProductAddedToCart(product1);
        viewModel.onProductAddedToCart(product2);
        assertEquals(totalPrice, viewModel.getCartTotal(), .009);
    }

    private static Product createDummyProduct() {
        return createDummyProduct(999.99);
    }

    private static Product createDummyProduct(double price) {
        return new Product("Product Title", 123, price, "5900135");
    }
}